const { Builder, By, Key, until } = require('selenium-webdriver');

(async function example(text) {

  var aux=[];

  let driver = await new Builder().forBrowser('chrome').build();
  try {
    await driver.get('http://www.google.com/ncr');
    await driver.findElement(By.name('q')).sendKeys(text, Key.RETURN);
    await driver.wait(until.titleIs(text + ' - Pesquisa Google'), 10000);
    await driver.wait(until.elementIsVisible(driver.findElement(By.id('resultStats'))), 10000);
    var result = await driver.findElement(By.id('resultStats')).getText();
    result = result.split(" ")[1];
    otherResult = await driver.findElements(By.className("LC20lb"));
     
    for (let i = 0; i < otherResult.length; i++) {
      const element = otherResult[i];
      aux.push(await element.getText())

    }
    var retorno = {}
    retorno.aux = aux;
    retorno.result = result;
    retorno.text = text;
    console.log(retorno);
  }
  finally {
     await driver.quit();
  }
})('outra coisa');
