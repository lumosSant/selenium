const { Builder, By, Key, until } = require('selenium-webdriver');
const Joi = require('joi');

class Produto{
    /**
     * Cria o meu produto do meu jeito
     *  @param {string} preco   string do preco do meu produto 
     */
    constructor(descricao, preco,precoD, url){
        Joi.assert(preco, Joi.string().required(), new Error('Esta faltando o PRECO'));
        Joi.assert(descricao,Joi.string().required(), new Error('Está faltando o nome do Produto'))
        Joi.assert(precoD, Joi.string().required(), new Error('Esta faltando o PRECOD'));
        Joi.assert(url, Joi.string().required(), new Error('Esta faltando a URL'));
        
        this.descricao = descricao;
        this.preco = Number(preco.replace("R$ ","").replace(/\./g,"").replace(",","."));
        this.precoD = Number(precoD.replace(" POR","").replace("DE R$ ","").replace(/\./g,"").replace(",","."))
        this.url = url;
        this.dif = 'R$ '+ (this.precoD - this.preco).toFixed(2)
    }
    
}

(async function Kabum() {
    let driver = await new Builder().forBrowser('chrome').build();
    try {
        await driver.get('https://www.kabum.com.br/');
        await driver.wait(until.titleIs('KaBuM! - Explosão de Preços Baixos!'), 2000);

        const ofertactn = await driver.findElement(By.className('oferta-ctn'));
        const ofertasHome = await ofertactn.findElements(By.className('ofertas-home'))
        for (let i = 0; i < ofertasHome.length; i++) {
            const section = ofertasHome[i];
            const img_src = await section.findElement(By.css('div.H-img a img')).getAttribute('src');
            const padding =  await section.findElement(By.className('ins-padding'));
            const preco =  await padding.findElement(By.className('H-preco')).getText();
            const precoD =  await padding.findElement(By.className('H-preco-D')).getText();
            const descricao = await padding.findElement(By.className('H-titulo oferta-nome')).getText();
            var prod = new Produto(descricao,preco,precoD,img_src)
            console.log(prod);
        }

    } catch (err) {
        console.log(err)
    } finally{
        await driver.quit();
    }
})();