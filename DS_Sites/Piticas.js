const { Builder, By, Key, until } = require('selenium-webdriver');
const Joi = require('joi');

class Produto {
    /**
     * Cria o meu produto do meu jeito
     *  @param {string} price   string do preco do meu produto 
     */
    constructor(title_catch, price, img, Parcelas) {
        Joi.assert(img, Joi.string().required(), new Error('Esta faltando a imagem'));
        Joi.assert(title_catch, Joi.string().required(), new Error('Está faltando o nome do Produto'))
        Joi.assert(price, Joi.string().required(), new Error('Esta faltando o PREÇO'));
        Joi.assert(Parcelas, Joi.string().required(), new Error('Esta faltando as Parcelas'));

        this.price = price;
        this.img = img;
        this.title_catch = title_catch;
        this.Parcelas = ('Em '+Qparce+'x '+'de '+Vparce);

    }
}

(async function Piticas() {
    let driver = await new Builder().forBrowser('chrome').build();
    try {
        //Home
        await driver.get('https://piticas.com.br/');
        await driver.wait(until.titleIs('Piticas'), 2000)
        const firstlock = await driver.findElement(By.className('tp-caption slidelink'));
        clicking = await firstlock.findElement(By.css('div a span')).click();
        await driver.sleep(1000)
        // Shopping
        const locking = await driver.findElement(By.className('ui three doubling cards centered category-grid'));
        let locked = await locking.findElements(By.className('position-mobile-card-product'))
        for (let i = 0; i < locked.length; i++) {
            const section = locked[i];
                // Catching Title
            const title_target = await section.findElement(By.className('dados'));
            const title_locked = await title_target.findElement(By.tagName('a'))
            const title_catch = await title_locked.getAttribute('title')

                // Catching Img
            const padding =  await section.findElement(By.className('line'));
            const img = await section.findElement(By.css('div img')).getAttribute('src');

                // Catching Price and Parce
            const price =  await padding.findElement(By.className('preco')).getText('innerHTML');
            const Qparce =  await padding.findElement(By.className('installmentMaxNumber')).getText('innerHTML');
            const Vparce =  await padding.findElement(By.className('installmentMaxValue')).getText('innerHTML');

            // let Parcelas = 'Em '+Qparce+'x '+'de '+Vparce;
            var prod = new Produto(descricao,preco,precoD,img)
            console.log(title_catch);
            
        }

    } catch (err) {
        console.log(err)
    } finally{
        await driver.quit();
    }
})();