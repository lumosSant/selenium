const { Builder, By, Key, until } = require('selenium-webdriver');

(async function Winn() {
    let driver = await new Builder().forBrowser('chrome').build();
    try {
        //login
        let login = 'acer@winn.com.vc'
        let pass = 'senha123'
        await driver.get('https://dev.winn.com.vc/login');
        await driver.wait(until.titleIs('Dashboard Winn'), 2000);
        await driver.findElement(By.id('inputEmail')).sendKeys(login);
        await driver.findElement(By.id('inputPass')).sendKeys(pass, Key.RETURN)
        await driver.sleep(10000)
        let value = '';
        const target = await driver.findElements(By.css('div.card-block'))
        for (let i = 0; i < target.length; i++) {
            section = target[i];
            await driver.sleep(1000)
            value = value + await section.getText('innerHTML');

        }

        console.log(value[1])

    } catch (err) {
        console.log(err)
    } finally {
        await driver.sleep(10000)
        await driver.quit();
    }
})();