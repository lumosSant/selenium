const { Builder, By, Key, until } = require('selenium-webdriver');
const Joi = require('joi');


function formatarCentavosNoNumero(price) {

    let tamanho = price.length;
    let tf = tamanho - 2;
    if (tf == 0) {
        return parseFloat('0.' + price)
    } else if (tf > 0) {
        return parseFloat(price.substr(0, tf) + '.' + price.substr(tf, 2)).toFixed(2)

    }
}

class Produto {
    /**
     * Cria o meu produto do meu jeito
     *  @param {string} price   string do preco do meu produto 
     */
    constructor(Titulo, price, img) {
        Joi.assert(img, Joi.string().required(), new Error('Esta faltando a imagem'));
        Joi.assert(Titulo, Joi.string().required(), new Error('Está faltando o nome do Produto'))
        Joi.assert(price, Joi.string().required(), new Error('Esta faltando o PREÇO'));


        this.Titulo = Titulo;
        this.img = img;
        this.price = formatarCentavosNoNumero(price);

    }

}




(async function Seibei() {
    let driver = await new Builder().forBrowser('chrome').build();
    try {
        await driver.get('https://seibei.com/');
        await driver.wait(until.titleIs('SEIBEI | t-shirts by David Murray and friends.'), 2000);

        const prodBox = await driver.findElement(By.id('product-loop'));
        const prod = await prodBox.findElements(By.className('product desktop-3 tablet-half mobile-3'))
        for (let i = 0; i < prod.length; i++) {
            const section = prod[i];
            const Titulo = await section.getAttribute('data-alpha')
            let price = await section.getAttribute('data-price')
            const img = await section.findElement(By.css('li a img')).getAttribute('src');

            let produto = new Produto(Titulo, price, img)
            console.log(produto)
            // console.log(produto.formatarCentavosNoNumero());
        }

    } catch (err) {
        console.log(err)
    } finally {
        await driver.quit();
    }
})();